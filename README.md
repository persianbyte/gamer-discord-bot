# Discord Gamer Bot

A discord bot to give your server info on games and more!(TBD)

## Features

- Shows the info on a given game based on the OpenCriticApi results.

## Requirements

- [Node](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Docker](https://www.docker.com/) (optional)

### Installation

```bash
# Clone the repository
git clone url

# Enter into the directory
cd discord-bot/

# Install the dependencies
npm install
```
### Configuration

Rename example_config.json to config.json

After cloning the project and installing all dependencies, you need to add your Discord API token and YouTube API KEY in the config.json file.

### Starting the discord bot

```bash

npm build

npm start
```

### Starting the bot using Docker

```bash
# Build the image
docker build --tag discordbot .

# Run the image
docker run -d discordbot
```

## Other Info

Every command will need to start with !atg or whatever you specify in your own config.

There's added easter eggs for my own server in the index.ts file, so remove them if necessary.
