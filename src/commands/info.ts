import { SlashCommandBuilder } from "@discordjs/builders";
import { CommandInteraction, Message } from "discord.js";
import { Command } from "../interfaces/command.interface";
import { OpenCriticService } from "../services/opencritic.service";


export default class Info implements Command {
    name: string = 'info';
    description: string = 'Get description, scores, genres, platforms, and trailers.';
    strArgs: string[] = ['game title'];

    data: SlashCommandBuilder = new SlashCommandBuilder()
	.setName(this.name)
	.setDescription(this.description);

    constructor() {
        this.data.addStringOption(option =>
            option.setName('title')
            .setDescription('give game title')
            .setRequired(true));
    }

    async execute(interaction: CommandInteraction) {
        
        try {

            if(!interaction.options && !interaction.options.getString("title")) return;

            let title = interaction.options.getString("title");
    
            const gameId = await OpenCriticService.getGameByTerm(title);
    
            if(!gameId || gameId.length == 0) {
                interaction.reply("Game does not exist or api is down.");
                return;
            }
            
            const game = await OpenCriticService.getGameById(gameId[0].id);
    
    
            if(!game) await interaction.reply("Game does not exist or api is down.");
            
            let reply = `Here's info on **${game.name}** \n`;
    
    
            reply += `Open Critic Url: ${OpenCriticService.openCriticSiteUrl}/game/${game.id}/${game.name.replace('&', '%26').replace("#", "%23")}\n`
    
            if(game.description) {
                reply += `Description: ${game.description} \n`
            }
            if(game.firstReleaseDate) {
                let releaseDate = new Date(game.firstReleaseDate);
                reply += `Release Date: ${releaseDate.toDateString()}\n`;
            }
            if(game.percentRecommended) {
                reply += `Recommended %: ${game.percentRecommended.toFixed(2)}\n`;
            }
            if(game.medianScore) {
                reply += `Median score: ${game.medianScore.toFixed(2)}\n`;
    
            }
            if(game.averageScore) {
                reply += `Average score: ${game.averageScore.toFixed(2)}\n`;
            }
    
            if(game.Platforms && game.Platforms.length > 0) {
                let platforms: Array<any> = game.Platforms;
                reply += `Platforms: ${ platforms.map(p => p.name as string).join(', ')}  \n`
            }
    
            if(game.Genres && game.Genres.length > 0) {
                let genres: Array<any> = game.Genres;
                reply += `Genres: ${ genres.map(g => g.name as string).join(', ')}  \n`
            }
    
            if(game.trailers && game.trailers.length > 0) {
                reply += `Trailer: ${game.trailers[0].externalUrl} \n`
            }
    
            console.log(reply.length);
            if(reply.length > 1900) {
                await interaction.reply(reply.substr(0, 1900));
                await interaction.channel.send(reply.substr(1900, reply.length - 1900));
            }
            else {
                await interaction.reply(reply);
            }
        }
        catch(e) {
            console.log(e);
            await interaction.reply('Error has occured while trying to fetch the your game info.')
        }
        

    }
}