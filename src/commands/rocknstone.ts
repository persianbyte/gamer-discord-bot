import { SlashCommandBuilder } from '@discordjs/builders'
import { Channel, CommandInteraction, Message, TextChannel } from 'discord.js'
import { Command } from "../interfaces/command.interface"

let cfg = require('../../config.json')

export default class RockNStone implements Command {
    name: string = 'rocknstone'
    description: string = 'rock....and....stone'
    strArgs: string [] = []

    data: SlashCommandBuilder = new SlashCommandBuilder()
	.setName(this.name)
	.setDescription(this.description);

    async execute(interaction: CommandInteraction) {
        await interaction.reply(`<:RocknStone:780970002274189313> ${cfg.rockNStoneQuotes[Math.floor(Math.random() * cfg.rockNStoneQuotes.length)]} <:RocknStone:780970002274189313>`)
    }

}