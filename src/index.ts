import {  Client, Intents } from 'discord.js'
import {REST} from '@discordjs/rest';
import {Routes} from 'discord-api-types/v9';

import fs from "fs";
import path from 'path'

import { Command } from './interfaces/command.interface'

let cfg = require('./../config.json')

const commands: Command[] = []

const commandFiles = fs.readdirSync(path.resolve(__dirname, 'commands')).filter(file => file.endsWith('.js'));

commandFiles.forEach(file => {
  let commandClass = require(`./commands/${file}`).default
  if(commandClass) {
    const command = new commandClass() as Command
    commands.push(command);
  }
})

const client: Client = new Client(
  {intents: [Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS, Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.DIRECT_MESSAGES],
   allowedMentions: { parse: ['users', 'roles'], repliedUser: true}});


   const rest = new REST({ version: '9' }).setToken(cfg.discordToken);

   (async () => {
     try {
       console.log('Started refreshing application (/) commands.');
   
       await rest.put(
         Routes.applicationCommands(cfg.discordAppId),
         { body: commands.map(c => c.data.toJSON()) },
       );
       
   
       console.log('Successfully reloaded application (/) commands.');
     } catch (error) {
       console.error(error);
     }
   })()
   


client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
  
    //client.user.setPresence({activities: [{ name: '!atg help', type: "PLAYING" }], status: 'online'});

});

client.on('interactionCreate', async interaction => {
  console.log(interaction);
  if (!interaction.isCommand()) return;

  const command = commands.find(c => c.name === interaction.commandName);

	if (!command) return;

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
	}
});

client.on('messageCreate', async msg => {
    if(msg && !msg.author.bot && msg.channel.type === 'GUILD_TEXT' && msg.type === 'DEFAULT' ) {
           //This is a fun area for my own server. Rock N Stone 4 life!
           msg.channel.messages.fetch({ limit: 3 }).then(messages => {
            let mArray = messages.map(m => m);
            if(messages && mArray.length > 2) {
              if(mArray[0].content.indexOf("<:RocknStone:780970002274189313>") > -1 && !mArray[1].author.bot && mArray[1].content.indexOf("<:RocknStone:780970002274189313>") > -1) {
                msg.channel.send(`<:RocknStone:780970002274189313> ${cfg.rockNStoneQuotes[Math.floor(Math.random() * cfg.rockNStoneQuotes.length)]} <:RocknStone:780970002274189313>`)
              } else if(msg.content.toLowerCase() === 'good bot' && mArray[1].author.bot) {
                msg.reply("https://tenor.com/bboYL.gif");
              } else if(msg.content.toLowerCase() === 'bad bot' && mArray[1].author.bot) {
                msg.reply("https://tenor.com/9dIE.gif")
              }
            }
          }
          );
    }
  });

  client.login(cfg.discordToken);