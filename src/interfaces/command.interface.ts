import { SlashCommandBuilder } from '@discordjs/builders';
import { Channel, CommandInteraction, Message } from 'discord.js';

export interface Command {
    name: string;
    description: string;
    strArgs: string [];
    data: SlashCommandBuilder;
    
    execute(interaction: CommandInteraction): Promise<any>;
}