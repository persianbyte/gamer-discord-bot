
import Axios from 'axios';

let cfg = require('../../config.json')

export class OpenCriticService {
    
    static openCriticSiteUrl: string = cfg.openCriticUrl;

    static async getGameByTerm(term: string) {
        const { data } = await Axios.get(`${cfg.openCriticApi}/game/search?criteria=${term.replace('&', '%26').replace("#", "%23")}`)
        return data;
    }

    static async getGameById(id: number) {
        const { data } = await Axios.get(`${cfg.openCriticApi}/game/${id}`)
        return data;
    }
}